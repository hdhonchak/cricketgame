package Helper;

public class PlayerStats implements Stats {



    int score ;
    int numberOfWicketTaken ;
    int numberOfWicketGiven ;
    @Override
    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public int getNumberOfWicketTaken() {
        return numberOfWicketTaken;
    }

    public void setNumberOfWicketTaken(int numberOfWicketTaken) {
        this.numberOfWicketTaken = numberOfWicketTaken;
    }

    @Override
    public int getNumberOfWicketGiven() {
        return numberOfWicketGiven;
    }

    public void setNumberOfWicketGiven(int numberOfWicketGiven) {
        this.numberOfWicketGiven = numberOfWicketGiven;
    }

}

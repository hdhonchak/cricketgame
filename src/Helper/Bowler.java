package Helper;

import Actions.Batting;
import Actions.BowlerBatting;

public class Bowler implements Player {

    int id ;
    String name ;

    private Batting batting ;
    public Bowler(){
    }

    public Bowler(int id, String name) {
        this.id = id;
        this.name = name;
        batting = new BowlerBatting();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int performBatting() {
        return batting.performBatting();
    }
}

package Helper;

public class TeamStats implements Stats{

    int score ;
    int numberOfWicketTaken ;
    int numberOfWicketGiven ;

    @Override
    public int getScore() {
        return score;
    }

    @Override
    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public int getNumberOfWicketTaken() {
        return numberOfWicketTaken;
    }

    @Override
    public void setNumberOfWicketTaken(int numberOfWicketTaken) {
        this.numberOfWicketTaken = numberOfWicketTaken;
    }

    @Override
    public int getNumberOfWicketGiven() {
        return numberOfWicketGiven;
    }

    @Override
    public void setNumberOfWicketGiven(int numberOfWicketGiven) {
        this.numberOfWicketGiven = numberOfWicketGiven;
    }


}

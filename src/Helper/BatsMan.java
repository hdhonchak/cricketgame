package Helper;

import Actions.BatsManBatting;
import Actions.Batting;

public class BatsMan implements Player {

    private int id ;
    private String Name;

    private Batting batting ;

    public BatsMan(){

    }

    public BatsMan(int id, String name) {
        this.id = id;
        Name = name;
        batting = new BatsManBatting();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public int performBatting() {
        return batting.performBatting();
    }
}

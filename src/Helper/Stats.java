package Helper;

public interface Stats {


    int getScore();
    int getNumberOfWicketTaken();

    int getNumberOfWicketGiven();

    void setNumberOfWicketTaken(int wicketTaken);

    void setNumberOfWicketGiven(int wicketGiven);

    void setScore(int score);


}

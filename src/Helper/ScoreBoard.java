package Helper;

import java.util.Map;

public class ScoreBoard {


    ScoreBoard(){
    }


    public static void print(Map<Integer , Stats> teamStats , Team team){

        int key = team.getId();
        Stats stats = teamStats.get(key);
        System.out.println(team.getName() +"          "+stats.getScore()+"          "+stats.getNumberOfWicketGiven());

    }


    public static void printPlayerStats(Map<Integer , Stats> teamStats, Player player){

        int key = player.getId();
        Stats stats = teamStats.get(player.getId());
        System.out.println(player.getName()+" out at "+ stats.getScore());

    }



}

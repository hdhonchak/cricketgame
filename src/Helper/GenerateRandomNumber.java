package Helper;

public class GenerateRandomNumber {


    final private int[] batsMan = {0,1,1,1,1,2,2,2,3,3,3,4,4,6,6,7} ;
    final private int[] bowler ={0,0,1,1,2,2,3,4,6,7};
    final private int [] toss = {0,1};
    final private int[] default_run = {0,1,2,3,4,5,6,7};
    private int min =0;
    private int max;

    private int[] play ;


    /*
        Perform Actions.Batting on the Basis of Players speciality.
     */

    public GenerateRandomNumber(Player player) {
        if( player instanceof Bowler){
            this.play = bowler ;
        }
        else{
            this.play = batsMan;
        }

        this.max = play.length-1;

    }

    public GenerateRandomNumber(){
        this.max = 1 ;
        this.play = toss ;
    }

    public GenerateRandomNumber(int limit){
        if(limit > default_run.length){
            throw new IllegalStateException("Maximum Limit For Generator is 7");
        }

        this.max=limit;
        this.play=default_run;

    }

    public int getRandomNumber(){
        double a = Math.random()*(max-min+1)+min;
        return play[(int)a];
    }
}

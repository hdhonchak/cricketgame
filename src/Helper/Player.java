package Helper;

public interface Player {
      int getId();
      void setId(int id);
      String getName();
      void setName(String name);

      int performBatting();

}

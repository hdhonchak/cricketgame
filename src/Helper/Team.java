package Helper;

import Helper.Player;

import java.util.ArrayList;

public class Team {

    private int id ;
    private String name ;

    private ArrayList<Player> players;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public Team(int id ,String name, ArrayList<Player> players) {
        this.id = id ;
        this.name = name;
        this.players = players;
    }

}

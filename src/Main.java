import Controller.CricketGame;
import Helper.BatsMan;
import Helper.Bowler;
import Helper.Player;
import Helper.Team;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {


        Player player1 = new BatsMan(1,"Rahul");
        Player player2 = new Bowler(2,"Sachin");
        Player player3 = new BatsMan(3,"Gautam");
        Player player4 = new Bowler(4,"Virat");

        ArrayList<Player> team1 = new ArrayList<>();
        team1.add(player1);
        team1.add(player2);
        ArrayList<Player> team2 = new ArrayList<>();
        team2.add(player3);
        team2.add(player4);

        Team india = new Team(5,"India",team1);
        Team england = new Team(6,"England",team2);

        int overs = 20 ;


        CricketGame cricketGame = new CricketGame(india,england,20);
        cricketGame.toss();
        cricketGame.startGame();



    }
}
package Controller;

import Helper.*;

import java.util.HashMap;
import java.util.Map;

public class CricketGame {


    private Team team1 ;
    private Team team2 ;

    private int overs ;

    Map<Integer , Stats> teamOneStats ;
    Map<Integer, Stats> teamTwoStats ;

    private int tossResult ;

    public CricketGame(Team team1, Team team2, int overs) {
        this.team1 = team1;
        this.team2 = team2;
        this.overs = overs;
        teamOneStats = new HashMap<>();
        teamTwoStats = new HashMap<>();
    }

    public void initializeScoreBoard(){
        System.out.println("Helper.Team Name" +"  "+"Total Score"+"  "+"Wickets Lost");
        teamOneStats.put(team1.getId() , new TeamStats());
        teamTwoStats.put(team2.getId(), new TeamStats());

        for(int i=0;i<team1.getPlayers().size();i++){

            teamOneStats.put(team1.getPlayers().get(i).getId() , new PlayerStats());
            teamTwoStats.put(team2.getPlayers().get(i).getId(), new PlayerStats());
        }
    }

    public void toss(){
        GenerateRandomNumber generateRandomNumber = new GenerateRandomNumber(1);
        this.tossResult = generateRandomNumber.getRandomNumber();

        if(this.tossResult == 0){
            System.out.println(team1.getName() + " WON BATTING !! " + team2.getName() + " WILL BE BOWLING -_-");
        }
        else{
            System.out.println(team2.getName() + " WON BATTING !! " + team1.getName() + " WILL BE BOWLING -_-");
        }

    }

    public void play(Team team , Map<Integer , Stats> stats){

         int totalOverPlayer = 0 ;
         int index = 0 ;
         while(totalOverPlayer++ < overs && index < team.getPlayers().size()){
             for(int i=0;i<6;i++){
                 int battingResult = team.getPlayers().get(index).performBatting();
                 if(battingResult == 7){
                     stats.get(team.getId()).setNumberOfWicketGiven(stats.get(team.getId()).getNumberOfWicketGiven()+1);
                     ScoreBoard.printPlayerStats(stats , team.getPlayers().get(index));
                     index++;
                 }
                 else{

                    stats.get(team.getPlayers().get(index).getId()).setScore(stats.get(team.getPlayers().get(index).getId()).getScore() + battingResult);
                    stats.get(team.getId()).setScore(stats.get(team.getId()).getScore() + battingResult);
                    ScoreBoard.print(stats , team);

                 }

                 if(index >= team.getPlayers().size()) {
                     System.out.println( team.getName() + " ALL OUT !!");
                     break;
                 }
             }

             System.out.println("Overs Left : " + (overs - totalOverPlayer));


         }


    }


    public void startGame(){

        initializeScoreBoard();

                        if(tossResult == 0){
                            play(team1,teamOneStats);
                            play(team2,teamTwoStats);
                        }
                        else{
                            play(team2,teamTwoStats);
                            play(team1,teamOneStats);
                        }
    }


}

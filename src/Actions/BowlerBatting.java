package Actions;
import Helper.*;
public class BowlerBatting implements Batting{


    final GenerateRandomNumber generateRandomNumber = new GenerateRandomNumber(new Bowler());

    @Override
    public int performBatting() {
        return generateRandomNumber.getRandomNumber();
    }
}

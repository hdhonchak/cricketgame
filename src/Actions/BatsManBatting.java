package Actions;

import Helper.*;


public class BatsManBatting implements Batting{


    final GenerateRandomNumber generateRandomNumber  =  new GenerateRandomNumber(new BatsMan());
    @Override
    public int performBatting() {
        return generateRandomNumber.getRandomNumber();
    }
}
